package book.ch1;
import cse131.ArgsProcessor;
import sedgewick.*;
/*************************************************************************
 *  Compilation:  javac BouncingBall.java
 *  Execution:    java BouncingBall
 *  Dependencies: StdDraw.java
 *
 *  Implementation of a 2-d bouncing ball in the box from (-1, -1) to (1, 1).
 *
 *  % java BouncingBall
 *
 *************************************************************************/

public class BouncingBall { 
    public static void main(String[] args) {
    	
    	ArgsProcessor ap = new ArgsProcessor(args);
    	int pause = ap.nextInt("Enter pause time:");

        // set the scale of the coordinate system
        StdDraw.setXscale(-1.0, 1.0);
        StdDraw.setYscale(-1.0, 1.0);

        // initial values
        double rx =Math.random(), ry = Math.random();     // position
        double vx = 0.015, vy = 0.023;     // velocity
        double radius = 0.05;              // radius
        double rx2 = Math.random(), ry2 = Math.random();
        double vx2 = 0.08, vy2 = 0.1;
        double radius2 = 0.1;
        double rx3 = Math.random(), ry3 = Math.random();
        double vx3 = 0.010, vy3 = 0.050;
        double radius3 = 0.25;
        double dist = Math.sqrt((rx - ry)*(rx - ry)) + ((rx2 - ry2)*(rx - ry));

        // main animation loop
        while (true)  { 

            // bounce off wall according to law of elastic collision
            if (Math.abs(rx + vx) > 1.0 - radius) vx = -vx;
            if (Math.abs(ry + vy) > 1.0 - radius) vy = -vy;
            if (Math.abs(rx2 + vx2) > 1.0 - radius) vx2 = -vx2;
            if (Math.abs(ry2 + vy2) > 1.0 - radius) vy2 = -vy2;
            if (Math.abs(rx3 + vx3) > 1.0 - radius) vx3 = -vx3;
            if (Math.abs(ry3 + vy3) > 1.0 - radius) vy3 = -vy3;
            if (dist == radius + radius2) {
            	
            }

            // update position
            rx = rx + vx; 
            ry = ry + vy; 
            rx2 = rx2 + vx2; 
            ry2 = ry2 + vy2;
            rx3 = rx3 + vx3; 
            ry3 = ry3 + vy3; 

            // clear the background
            StdDraw.setPenColor(StdDraw.GRAY);
            StdDraw.filledSquare(0, 0, 1.0);

            // draw ball on the screen
        	StdDraw.setPenColor(StdDraw.BLACK); 
            StdDraw.filledCircle(rx, ry, radius); 

        	StdDraw.setPenColor(StdDraw.GREEN); 
            StdDraw.filledCircle(rx2, ry2, radius); 
            
            StdDraw.setPenColor(StdDraw.BLUE); 
            StdDraw.filledCircle(rx3, ry3, radius); 
            
            
           

            // display and pause for 20 ms
            StdDraw.show(pause); 
        } 
    } 
} 
