package exercises6;

public class FactorialNoBaseCase {
	
	public static int factorial(int n) {
		//Part 1
		//
		//Leeuwenhoek's speculation cannot be true do to the maximum amount of space in a woman's whom. 
		//There would be a maximum amount of children a woman could carry which is not true.
		//
		//Part 2
		//
		// no base case, therefore, the problem is going to keep going until it goes out of bounds
		// if (n == 0)
		//	return 0;
		// else
		return n * factorial(n-1);
	}

	public static void main(String[] args) {
		int ans = factorial(2);
		System.out.println("Answer is " + ans);
	}
}
