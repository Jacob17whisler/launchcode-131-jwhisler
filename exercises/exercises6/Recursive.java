package exercises6;

public class Recursive {

	public static boolean strcmp(String x, String y) {
		if (x.length() != y.length()) {
			return false;
		}
		for (int i = 0; i < x.length(); i++) {
			if (x.charAt(i) != y.charAt(i)) {
				return false;
			}
		}
		return true;
	}
	
	public static boolean strcmpR(String x, String y) {
		if (x.length() != y.length()) {
			return false;
		}
		return strcmpR2(x, y, 0);
	}
	
	public static boolean strcmpR2(String x, String y, int i) {
		if (i == x.length()) {
			return true;
		}
		else if (x.charAt(i) != y.charAt(i)) {
			return false;
		}
		else {
			return strcmpR2(x, y, i + 1);
		}
	}

}
