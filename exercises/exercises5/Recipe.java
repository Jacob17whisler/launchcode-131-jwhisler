package exercises5;

public class Recipe {

	public static void main(String[] args) {

		//
		// No java code needed for this exercise
		// Below, use comments or actual Java code
		//  to type in the abstractions
		//  you found in the recipe story
		// 

//		Stir and combine are both abstractions and heat could be the second abstraction.
		//
		// Use those abstractions in comments or code to 
		//   express the recipe
		//
		
//		You can take these abstractions and make them both mixWithSpoon (x, y, speed). 
//		stir is mixWithSpoon (x, y, medium) and combine is mixWithSpoon (x, y, slow).
//		heat (x, time, temp) would be the other abstraction.
	}

}
