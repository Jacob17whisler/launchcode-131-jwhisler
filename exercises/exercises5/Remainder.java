package exercises5;

public class Remainder {

	
		
		public static int remain(int a, int b) {
			int rem = a;
			if (a > b) {
				do {
					rem -= b;
				} while (rem > b);
			}
			return rem;
		}
		
		
		
		public static boolean isEven(int c) {
			if (remain(c, 2) > 0) {
				return false;
			}else {
				return true;
			}
		}
		
		
		public static void main(String[] args) {
			System.out.println(remain(10, 3) + " " + 10%3);
		}

	}


