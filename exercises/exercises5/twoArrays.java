package exercises5;

public class twoArrays {

	public static boolean twoArrays(int[] a, int[] b) {
		
		if (a.length != b.length) {
			return false;
		}
		
		for (int i = 0; i < a.length; i++) {
			if (a[i] != b[i]) {
				return false;
			}
		}
		
		return true;
	}
	
	//counting the number of times a character appears in a set of characters
	
	public static int letterCount(String s, char c) {
		int counter = 0;
		for (int i = 0; i < s.length(); i++) {
			
			if (s.charAt(i) == 'g' || s.charAt(i) == 'G'){
				counter++;
			}
		}
		return counter;
	}
	
	public static void main(String[] args) {
		System.out.println(twoArrays(new int[] {2, 4, 6}, new int[] {2, 4, 6}));
		System.out.println("The number of times 'G' is in a word " + letterCount(""));
	}
	
}


	