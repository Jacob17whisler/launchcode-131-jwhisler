package exercises3;

import java.awt.Color;

import sedgewick.StdDraw;

public class smiley {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		StdDraw.setPenColor(Color.YELLOW);
		StdDraw.filledCircle(0.5, 0.5, 0.5);
		
		StdDraw.setPenColor(Color.BLACK);
		StdDraw.circle(0.5, 0.5, 0.5);
		
		StdDraw.setPenRadius(0.08);
		StdDraw.point(0.35, 0.6);
		StdDraw.point(0.65, 0.6);
		
		StdDraw.filledCircle(0.5, 0.3, 0.2);
		StdDraw.setPenColor(Color.YELLOW);
		StdDraw.filledCircle(0.5, 0.4, 0.2);
		
		while (!StdDraw.mousePressed()) {
			StdDraw.pause(100);
		}
		
		if (StdDraw.mousePressed()) {
			StdDraw.setPenColor(Color.ORANGE);
			StdDraw.filledCircle(0.5, 0.5, 0.5);
			
			StdDraw.setPenColor(Color.BLACK);
			StdDraw.setPenRadius(0.08);
			StdDraw.point(0.35, 0.6);
			StdDraw.point(0.65, 0.6);
			
			StdDraw.filledCircle(0.5, 0.3, 0.2);
			StdDraw.setPenColor(Color.ORANGE);
			StdDraw.filledCircle(0.5, 0.4, 0.2);
		}
		

	}

}
