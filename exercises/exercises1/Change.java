package exercises1;

import cse131.ArgsProcessor;

public class Change {

	public static void main(String[] args) {
		//
		// Below, prompt the user to enter a number of pennies
		//
		ArgsProcessor ap = new ArgsProcessor(args);
		
		int numPennies = ap.nextInt("How many pennies do you have?:");
		
		//
		// Then, compute and print out how many 
		//    dollars, quarters, dimes, nickels, and pennies
		// should be given in exchange for those pennies, so as to
		// minimize the number of coins (see the videos)
		//
		int dollars = numPennies / 100;
		int afterDollarsChange = numPennies % 100;
		int quarters = afterDollarsChange / 25;
		int afterQuartersChange = numPennies % 25;
		int dimes = afterQuartersChange / 10;
		int afterDimesChange = afterQuartersChange % 10;
		int nickels = afterDimesChange / 5;
		int afterNickelsChange = afterDimesChange % 5;
		
		System.out.println("For " + numPennies + " Pennies: " + dollars + " dollars " + quarters + " quarters " + dimes + " dimes " + nickels + " nickels " + afterNickelsChange + " pennies");
	}

}


