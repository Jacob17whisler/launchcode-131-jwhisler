package exercises4;

import cse131.ArgsProcessor;
import sedgewick.StdDraw;

public class sineWave {

	public static void main(String[] args) {

		//use Math.pi for calculations
		//Set x scale from 0 to 2 * pi
		//set y scale from -1 to 1
		//ask for # of points
		//determine the distance of a single step (2 * Math.pi) / numSteps
		//for loop iteratively drawing points to make up sine wave
			//yValue = Math.sin(xValue)
			//draw point
		
		StdDraw.setXscale(0, 2 * Math.PI);
		StdDraw.setYscale(-1, 1);
		
		ArgsProcessor ap = new ArgsProcessor(args);
		int numPoints = ap.nextInt();
		double xDistance = 2 * Math.PI / numPoints;
		
		for (int i=0; i < numPoints; i++) {
			double xValue = xDistance * i;
			double yValue = Math.sin(xValue);

		StdDraw.point(xValue, yValue);
		}
		
		
	}

}
