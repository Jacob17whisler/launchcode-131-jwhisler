package exercises4;

import java.awt.Color;

import sedgewick.StdDraw;

public class GraphicsDemo {

	public static void main(String[] args) {
		// blue point (look carefully, will be very small on your screen)
		StdDraw.setPenColor(Color.BLUE);
		StdDraw.point(0.5, 0.5);
		// larger green point
		StdDraw.setPenColor(Color.GREEN);
		StdDraw.setPenRadius(0.2);
		StdDraw.point(0.8, 1);
		// unfilled red triangle
		StdDraw.setPenColor(Color.RED);
//		polygon would not work
//		StdDraw.polygon(0.4, 0.5);
		StdDraw.setPenRadius(0.001);
		StdDraw.line(0.2, 0.2, 0.5, 1);
		StdDraw.line(0.5, 1, 0.8, 0.8);
		StdDraw.line(0.8, 0.8, 0.2, 0.2);
		// yellow circle, filled
		StdDraw.setPenColor(Color.YELLOW);
		StdDraw.filledCircle(0.4, 0.8, 0.4);
		// filled blue rectangle
		StdDraw.setPenColor(Color.BLUE);
		StdDraw.filledSquare(0.2, 0.4, 0.5);


	}

}
