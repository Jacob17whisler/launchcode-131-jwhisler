package exercises4;

import cse131.ArgsProcessor;
import sedgewick.StdDraw;

public class sineWaveMotion {

	public static void main(String[] args) {

		//use Math.pi for calculations
		//Set x scale from 0 to 2 * pi
		//set y scale from -1 to 1
		//ask for # of points
		//determine the distance of a single step (2 * Math.pi) / numSteps
		//for loop iteratively drawing points to make up sine wave
			//yValue = Math.sin(xValue)
			//draw point
		
		StdDraw.setXscale(0, 2 * Math.PI);
		StdDraw.setYscale(-1, 1);
		
		ArgsProcessor ap = new ArgsProcessor(args);
		int numPoints = ap.nextInt();
		double xDistance = 2 * Math.PI / numPoints;
		double[] points = new double[numPoints];
		
		for (int i=0; i < numPoints; i++) {
			double xValue = xDistance * i;
			points[i] = Math.sin(xValue);

			//StdDraw.point(xValue, yValue);
		}
		
		//variable that is starting position
		//infinite while loop
			//clear
			//draw sine wave
				//for loop that draws all the points
					//draw a point
					//if statement checks if we've reached the end, and if so, resets x to 0
						//reset x to 0
				//increment starting position
				//if statement checking if starting position reaches end, and resets to 0
					//reset start to 0
			//show
		
		int xStart = 0;
		
		
		while (true) {
			StdDraw.clear();
			for (int i=0; i < numPoints; i++) {
				StdDraw.point(xDistance * i, points[xStart]);
				xStart++;
				if (xStart > numPoints - 1) {
					xStart = 0;
				}
			}
			xStart++;
			
			if (xStart > numPoints - 1) {
				xStart = 0;
			}
			StdDraw.show(20);
			
			
			
			
			
		}
	}

}
