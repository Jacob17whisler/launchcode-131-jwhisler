package exercises2;

import cse131.ArgsProcessor;

public class FeetInches {

	public static void main(String[] args) {
		//
		// Prompt the user for a number of inches
		//
		// Convert that into feet and inches
		//   BUT
		// Be sure to use the singular "foot" or "inch"
		//   where appropriate, as discussed in
		//   the introductory video
		//
		// For example, 61 inches would produce
		//    the output
		//   5 feet and 1 inch
		//

		ArgsProcessor ap = new ArgsProcessor(args);
		int inches = ap.nextInt("How many inches?");
		
		int feet = inches / 12;
		int inchesAfter = inches % 12;
		
//		System.out.println(feet + " feet and " + inchesAfter + " inches");
		
		if (inches < 24 && inchesAfter == 1) {
			System.out.println(feet + " foot and " + inchesAfter + " inch");
		}
		else {
			if (inches < 24) {
				System.out.println(feet + " foot and " + inchesAfter + " inches");
			}
			else {
				if (inchesAfter == 1) {
					System.out.println(feet + " feet and " + inchesAfter + " inch");
				}
				else {
					System.out.println(feet + " feet and " + inchesAfter + " inches");
				}
			}
		}
	}

}
