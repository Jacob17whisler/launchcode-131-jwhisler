package exercises2;

import java.math.BigInteger;

import cse131.ArgsProcessor;

public class Fibonacci {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ArgsProcessor ap = new ArgsProcessor(args);
		int N = ap.nextInt("How many numbers do you want to see?");
		BigInteger x = new BigInteger("0");
		BigInteger y = new BigInteger("1");
		
		System.out.println(x);
		System.out.println(y);
		
		for (int i = 1; i < (N - 2); i++) {
			x = x.add(y);
			y = y.add(y);
			
			// OR
//			int sum = x + y;
//			x = y;
//			y = sum;
//			System.out.println(sum);
			
			
			System.out.println(x);
			System.out.println(y);
			
		}
	}

}
