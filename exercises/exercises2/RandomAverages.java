package exercises2;

public class RandomAverages {

	public static void main(String[] args) {
		//
		// Write a loop that computes the average
		//     of 1, 2, ... 1000 doubles
		// In each iteration of that loop, print
		//     the average of the doubles generated
		//     thus far.
		//
		// Your code goes below here.
		
		int r = 1;
		double sum = 0.0;
		while (r < 1001) {
			sum = sum + Math.random();
			r = r + 1;
			System.out.println(sum / r);
		}
		
		
		//
		//  Some questions:
		//    1) How does the average change as your loop progresses?
		//    2) Can you write code in your loop that prints out the average only
		//         every 100 iterations, instead of each iteration?
		//	1) The average should get closer and closer to 5
		//  2) If you add an if statement stating to only print every 100  iterations.
		
	}

}
