package exercises2;

import cse131.ArgsProcessor;

public class Integer {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ArgsProcessor ap = new ArgsProcessor(args);
		int one = ap.nextInt("Type a whole number");
		int two = ap.nextInt("Type another whole number");
		
		if (one > two) {
			System.out.println("The first number is bigger");
		}
		else if (two > one) {
			System.out.println("The second number is bigger");			
		}
		else {
			System.out.println("Both numbers are the same");
		}
	}

}
