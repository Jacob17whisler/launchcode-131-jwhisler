package exercises2;

import cse131.ArgsProcessor;

public class Asterisk {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ArgsProcessor ap = new ArgsProcessor(args);
		
		int x = ap.nextInt("Number of rows?");
		int y = ap.nextInt("Number of columns?");
		for (int i = 0; i < x; i++) {
			for (int j = 0; j < y; j++) {
				System.out.print("*");
			}
		}
		
	}

}
