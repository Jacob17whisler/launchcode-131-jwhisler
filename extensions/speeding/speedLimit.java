package speeding;

import cse131.ArgsProcessor;

public class speedLimit {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ArgsProcessor ap = new ArgsProcessor(args);
		int speed = ap.nextInt("What was the speed you were going? ");
		int limit = ap.nextInt("What was the speed limit? ");
		int diff = speed - limit;
		int fine = (diff >= 10) ? 50 + 10 * (diff - 10) : 0;
		
		System.out.println(fine);
		
	}

}
