package studio2;

import cse131.ArgsProcessor;

public class GamblersRuin {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ArgsProcessor ap = new ArgsProcessor(args);
		
		
		double inAmount = 12.0;// ap.nextDouble("Amount of money to start with");
		double winChance = 0.25;//ap.nextDouble("Probability to win a gamble");
		double winAmount = 15.0;//ap.nextDouble("If you reach this amount of money, you won");
		double totalPlays = 31.0;//ap.nextInt("Number of simulations to run");
		String outcome = "LOSE";
		
		double Nlosses = 0;

		winChance = Math.round(winChance*100.0)/100.0;
		
		double ruin = 0.0;

		for (int i = 0; i < totalPlays; i++)
		{
			double startAmount = inAmount;
			
			int increment = 0;
			
			while ((startAmount > 0) && (startAmount < winAmount))
			{
				increment++;
				
				double rando = Math.round(Math.random()*100.0)/100.0;

				if	(winChance >= rando){
					startAmount++;
				} else {
					startAmount--;
				}
				
				double lossChance = 1-winChance;

				 ruin = 0.0;
				if (lossChance == winChance)
				{
					ruin = (Math.pow(lossChance/winChance,startAmount)-
							Math.pow(lossChance/winChance,winAmount))/(1-Math.pow(lossChance/winChance, winAmount));
				} else {
					ruin = 1 - startAmount/winAmount;
				}
				
				if (startAmount >= winAmount)
				{
					outcome = "WIN";
					break;
				} else if (startAmount == 0)
				{
					outcome = "LOSE";
					Nlosses++;
					break;
				}
			
			}

			System.out.println("Simulation "+ i + ":  " + increment + "  rounds\t\t" + outcome);
		}

		System.out.println("Losses: " + Nlosses + "  Simulations: " + totalPlays);
		System.out.println("Actual Ruin Rate: " + Nlosses/totalPlays +  "  Expected Ruin Rate: " + ruin);

	}	
}
		
	