package studio7;

public class Rectangle {
	
	private double width;
	private double length;
	private double area;
	private double perm;
	private double same;
	
	

	public Rectangle(double width, double length) {
		super();
		this.width = width;
		this.length = length;
	}
	
	public void area() {	
		this.area = this.width * this.length;
	}
	
	public void prem() {
		this.perm = this.width * 2 + this.length * 2;
	}
	
	public void square() {
		if (this.width == this.length) {
			System.out.println("This rectangle is a square");
		}
	}
	
	public boolean same(Rectangle rec2) {
		return this.width * this.length > rec2.width * rec2.length; 
	}



	public static void main(String[] args) {
		Rectangle rec1 = new Rectangle(15, 25);
		Rectangle rec2 = new Rectangle(5, 5);
		rec2.square();
		System.out.println("Are the two rectangles equal? " + rec1.same(rec2));

	}

}
