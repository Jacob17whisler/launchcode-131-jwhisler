package studio7;

public class FranklinTheCat {
	
	private String name;
	private int age;
	private int lives;
	private String furColor;
	private int fur;
	private double weight;
	
	public FranklinTheCat(String name, String furColor, double weight, int fur) {
		this.name = name;
		this.age = 0;
		this.lives = 9;
		this.furColor = furColor;
//		this.fur = 1000000;
		this.weight = weight;
		
	}
	
	public void increasingAge() {
		this.age = this.age + 1;
	}
	
	public void shed() {
		this.fur = this.fur - 1000;
		
	}
	
	public void shed(int fursToLose) {
		this.fur = this.fur - fursToLose;
	}
	
	public boolean die() {
		if (lives != 0) {
			this.lives = this.lives - 1;
			return true;
		} else {
			return false;
		}
	}
	
	public void meow() {
		System.out.println("MEOW!!");
	}
	
	public void modifyweight(double change) {
		this.weight = this.weight + change;
	}
	
	public void convo(FranklinTheCat secondCat) {
		
	}

	public static void main(String[] args) {
		double[] array = new double[500];
		FranklinTheCat cat1 = new FranklinTheCat("cat1", "Orange", 5, 1000000);
		FranklinTheCat cat2 = new FranklinTheCat("cat2", "Black", 10, 100000);
		
		System.out.println(cat1.age);
		cat1.increasingAge();
		System.out.println(cat1.age);
		System.out.println(cat1.furColor);
	}
	
		FranklinTheCat firstCat = new FranklinTheCat("firstCat", "brown", 10, 1000000);
		while (firstCat.fur > 0) {
			firstCat.shed(100000);
			System.out.println("Just lost 100,000 furs, we now have  " + firstCat.fur);
		}
		FrankliinTheCat secondCat = new FranklinTheCat("secondCat", "black", 5, 1000000);
		
		while (firstCat.lives != 0 && secondCat.lives != 0) {
			firstCat.meow();
			if (Math.random() < 0.5) {
				firstCat.lives -= 1;
				System.out.println("firstCat lost a life");
			}
			secondCat.meow();
			if (Math.random() < 0.5) {
				secondCat.lives -= 1;
				System.out.println("secondCat lost a life");
			}
		}
		
		

}
