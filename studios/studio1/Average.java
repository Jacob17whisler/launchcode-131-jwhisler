package studio1;

import cse131.ArgsProcessor;

public class Average {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ArgsProcessor ap = new ArgsProcessor(args);
		
		int numOne = ap.nextInt("Input a number: ");
		int numTwo = ap.nextInt("Input another number: ");
		
		double ave = (numOne + numTwo) / 2.0;
		
		System.out.println(ave);
	}

}
