package studio3;

import cse131.ArgsProcessor;

public class sieveEratosthenes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ArgsProcessor ap = new ArgsProcessor(args);
		int n = ap.nextInt("What is the size of the Array");
		int[] sieve = new int[n];
		
		for (int i = 0; i < n; i++) {
			sieve[i] = i;
//			System.out.println(sieve[i]);
//			if ( sieve[i] % 2 == 0) {
//				System.out.println(i + " is not prime");
//			}
		}
		
		for (int i = 2; i < n; i++) {
			for (int j = i + 1; j < n; j++) {
				if ( sieve[j] % i == 0) {
					sieve[j] = 0;
					
				}
			}
			
		}
		System.out.print("Primes under " + n + " = ");
				
		for (int i = 0; i < n; i++) {
			if (sieve[i] == 0) {
				System.out.print("");
			}
			else {
				 System.out.print(sieve[i] + ", ");
			}
		}

	}

}
