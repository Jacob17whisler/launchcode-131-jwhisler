package lab1;

import cse131.ArgsProcessor;

public class Nutrition {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArgsProcessor ap = new ArgsProcessor(args);
		String name = ap.nextString("Food: ");
		double carbs = ap.nextDouble("Grams of Carbs: ");
		double fat = ap.nextDouble("Grams of fat: ");
		double protein = ap.nextDouble("Grams of protein: ");
		int statedCals = ap.nextInt("Calories stated: ");

		
		
		double carbsCal = carbs * 4.0;
		
		
//using factors of 10 to Math.round to a certain decimal point!!!!!
		double fatCal = Math.round(10.0 * (fat * 9.0)) / 10.0; 
		
		double proteinCal = protein * 4.0;
		double otherCal =Math.round(10.0 * ((carbsCal + fatCal + proteinCal) - statedCals)) / 10.0;
		double fiber = Math.round(100.0 * (otherCal / 4.0)) / 100.0;

		
		double percentCarbs = Math.round(1000.0 * (carbsCal / statedCals)) / 10.0;
		double percentFat = Math.round(1000.0 * (fatCal / statedCals)) / 10.0;
		double percentProtein = Math.round(1000.0 * (proteinCal / statedCals)) / 10.0;

		boolean lowCarb = percentCarbs <= 25;
		boolean lowFat = percentFat <= 15;
		double random = Math.random();
		boolean heads = (random > 0.5); 
		
		
				System.out.println(name + " has " + carbs + " grams of carbohydrates = " + carbsCal + " Calories. "
						+ fat + " Grams of fat = " + fatCal + " Calories. " + protein + " grams of protein = "
						+ proteinCal + " Calories");
		System.out.println("This food is said to have " + statedCals + " (available) Calories. \nWith " + otherCal
				+ " unavailable Calories, this food has " + fiber + " grams of fiber.\n\n Approximetely\n\t"
				+ percentCarbs + "% of your food is carbohydrates\n\t" + percentFat + "% of you food is fat\n\t"
				+ percentProtein + "% of you food is protein\n\n This food is acceptable for a low-carb diet? "
				+ lowCarb + "\n This food is acceptable for a low-fac diet? " + lowFat
				+ "\n By coin flip, you should eat this food? " + heads);

	}

}
