

package lab2;

public class RPS {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		
		double player1 = 0;
		double rock1 = 0;
		double paper1 = 1;
		double scissors1 = 2;
		double rock2 = 0;
		double paper2 = 1;
		double scissors2 = 2;
		int i = 0;
		int player1win = 0;
		int player2win = 0;
		int playersTie = 0;
		int totalPlays = 0;
		
		
		for (int t = 0; t < 1000; t++) {
			totalPlays++;
			double player2 = Math.random();
			if (i == 3) {
				i = 0;
				
			}
//					if (i == 0) {
						player1 = i;
//						System.out.println("Player 1 chose " + i);
//					}
//					else {
//						if (i == 1) {
//							player1 = paper1;
//							System.out.println("Player 1 chose Paper");
//						}
//						else {
//							player1 = scissors1;
//							System.out.println("Player 1 chose Scissors");
//						}
//					}
				
				
				if (player2 <= 1.0/3.0) {
//					System.out.println("Player 2 chose Rock");
					player2 = rock2;
				}
				else {
					if (player2 <= 2.0/3.0 && player2 > 1.0/3.0) {
//						System.out.println("Player 2 chose Paper");
						player2 = paper2;
					}
					else
//						System.out.println("Player 2 chose Scissors");
						player2 = scissors2;
				}
			
			
	
			if (player1 == rock1 && player2 == paper2) {
				System.out.println("Player 2 wins!");
				player2win++;
			}
			else {
				if (player1 == rock1 && player2 == scissors2) {
					System.out.println("Player 1 wins!");
					player1win++;
				}
				else {
					if (player1 == paper1 && player2 == rock2) {
						System.out.println("Player 1 wins!");
						player1win++;
					}
					else {
						if (player1 == paper1 && player2 == scissors2) {
							System.out.println("Player 2 wins!");
							player2win++;
						}
						else {
							if (player1 == scissors1 && player2 == rock2) {
								System.out.println("Player 2 wins!");
								player2win++;
							}
							else {
								if (player1 == scissors1 && player2 == paper2) {
									System.out.println("Player 1 wins!");
									player1win++;
								}
								else {
									System.out.println("It is a tie");
									playersTie++;
								}
							}
						}	
					}
				}
			}
		i++;	
		}
		System.out.println("Player One win ratio: " + player1win + "/" + totalPlays + "\t\t Player Two win ratio: " + player2win + "/" + totalPlays + "\t\t Tie Ratio: " + playersTie + "/" + totalPlays);
	}
}
