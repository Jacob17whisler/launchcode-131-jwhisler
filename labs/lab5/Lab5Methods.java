package lab5;

public class Lab5Methods {

	public static int sumDownBy2(int n) {
		if (n <= 1) {
			return n;
		}
		else {
			return n + sumDownBy2(n-2);
		}
	}
	
	public static double harmonicSum(int n) {
		if (n <= 1) {
			return 1;
		}
		else {
			return 1.0 / n + harmonicSum(n-1);
		}
	}
	
	public static double geometricSum(int k) {
		double sum = 1;
		for ( int i = 1; i <= k; i++) {
			sum = sum + (1.0/Math.pow(2, i));
		}
		return sum;
	}
	
	public static int multPos(int j, int k) {
		if (k <= 0) {
			return 0;
		}
		else {
			return j + multPos(j, k-1);
		}
	}
	
	public static int mult(int j, int k) {
		double ans = multPos(Math.abs(j),Math.abs(k));
		if (j < 0 || k < 0) {
			ans = -ans;
			if (j < 0 && k < 0) {
				ans = -ans;
			}
		}
		return (int)ans;
		
	}
	
	public static int expt(int n, int k) {
		if (k <= 0) {
			return 1;
		}
		else {
			return n * expt(n, k-1);
		}
	}


}
