package lab4;

import cse131.ArgsProcessor;
import sedgewick.StdDraw;

public class BumpingBalls {

	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args);
		int pause = ap.nextInt("Enter pause time:");
		int numBalls = ap.nextInt("Enter the number of balls");

		StdDraw.setXscale(-1.0, 1.0);
		StdDraw.setYscale(-1.0, 1.0);

		double[] rx = new double [numBalls];
		double[] ry = new double [numBalls];
		double[] vx = new double [numBalls];
		double[] vy = new double [numBalls];
		//        double rx =Math.random(), ry = Math.random();
		//        double vx = 0.015, vy = 0.023;
		double radius = 0.05;
		for (int i = 0; i < numBalls; i++) {
			rx[i] = Math.random();
			ry[i] = Math.random();
			vx[i] = Math.random() * 0.07;
			vy[i] = Math.random() * 0.07;
			
		}
		//        double rx2 = Math.random(), ry2 = Math.random();
		//        double vx2 = 0.08, vy2 = 0.1;
		//        double rx3 = Math.random(), ry3 = Math.random();
		//        double vx3 = 0.010, vy3 = 0.050;
		//        double radius3 = 0.25;

		//        double dist2 = Math.sqrt((rx2 - rx3)*(rx2 - rx3)) + ((ry2 - ry3)*(ry2 - ry3));
		//        double dist3 = Math.sqrt((rx - rx3)*(rx - rx3)) + ((ry - ry3)*(ry - ry3));


		while (true)  { 
			StdDraw.clear();
			for (int i = 0; i < numBalls; i++) {
				for (int j = i+1; j < numBalls; j++) {
					double diffx = Math.abs(rx[i]-rx[j]);
					double diffy = Math.abs(ry[i]-ry[j]);
					double dist = Math.sqrt(Math.pow(diffx, 2) + Math.pow(diffy, 2));
					if (dist <= 2*radius) {
						double tempX = vx[i];
						double tempY = vy[i];
		            	vx[i] = vx[j];
		            	vy[i] = vy[j];
		            	vx[j] = tempX;
		            	vy[j] = tempY;
		            	/*
						vx[i]=-vx[i];
						vy[j]=-vy[j];
						vx[j]=-vx[j];
						vy[i]=-vy[i];
						*/
					}
				}
				if (Math.abs(rx[i] + vx[i]) > 1.0 - radius) vx[i] = -vx[i];
				if (Math.abs(ry[i] + vy[i]) > 1.0 - radius) vy[i] = -vy[i];		
				//            if (Math.abs(rx2 + vx2) > 1.0 - radius2) vx2 = -vx2;
				//            if (Math.abs(ry2 + vy2) > 1.0 - radius2) vy2 = -vy2;
				//            if (Math.abs(rx3 + vx3) > 1.0 - radius3) vx3 = -vx3;
				//            if (Math.abs(ry3 + vy3) > 1.0 - radius3) vy3 = -vy3;
	            
				//            	vx2 = -vx2;
				//            	vy2 = -vy2;
				//            }


				rx[i] = rx[i] + vx[i]; 
				ry[i] = ry[i] + vy[i]; 
				//            rx2 = rx2 + vx2; 
				//            ry2 = ry2 + vy2;
				//            rx3 = rx3 + vx3; 
				//            ry3 = ry3 + vy3; 

				

				StdDraw.setPenColor(StdDraw.BLUE); 
				StdDraw.filledCircle(rx[i], ry[i], radius); 

				//				StdDraw.setPenColor(StdDraw.GREEN); 
				//				StdDraw.filledCircle(rx2, ry2, radius); 
			}
			//            StdDraw.setPenColor(StdDraw.BLUE); 
			//            StdDraw.filledCircle(rx3, ry3, radius); 

			StdDraw.show(pause); 

		} 
	}
}
