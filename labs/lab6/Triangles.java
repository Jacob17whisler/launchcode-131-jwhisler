package lab6;

import java.awt.Color;

import sedgewick.StdDraw;

public class Triangles {
	
	public static void triangle(double x1,double x2,double x3,double y1,double y2,double y3) {
		if (Math.abs(x1 - x2) < 0.001) {
			return;
		}
		StdDraw.setPenColor(Color.BLACK);
		StdDraw.filledPolygon(new double[]{x1,x2,x3},new double[]{y1,y2,y3});
		triangle(x1/2,x2/2,x3/2,y1/2,y2/2,y3/2);
		triangle((1+x1)/2,(1+x2)/2,(1+x3)/2,y1/2,y2/2,y3/2);
		triangle((x1/2)+.25,(x2/2)+.25,(x3/2)+.25,(1+y1)/2,(1+y2)/2,(1+y3)/2);
	}
	
	public static void main(String[] args) {
		StdDraw.setPenColor(Color.WHITE);
		StdDraw.filledPolygon(new double[]{0, 1, 0.5},new double[]{0, 0, 1});
		triangle(.25, .5, .75, .5,0,.5);
	}
}
