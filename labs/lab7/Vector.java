package lab7;

public class Vector {
	
	private double xCoord, yCoord;
	
	
	public Vector (double x, double y){
		xCoord = x;
		yCoord = y;
	}
	
	public double getDeltaX() {
		return xCoord; 
	}
	
	public double getDeltaY() {
		return yCoord;
	}
	
	public Vector plus(Vector v) {
		double newX, newY;
		newX = this.getDeltaX() + v.getDeltaX();
		newY = this.getDeltaY() + v.getDeltaY();
		return new Vector(newX, newY);
	}
	
	public Vector minus(Vector v) {
		double newX, newY;
		newX = this.getDeltaX() - v.getDeltaX();
		newY = this.getDeltaY() - v.getDeltaY();
		return new Vector(newX, newY);
	}
	
	public Vector scale(double v) {
		double newX, newY;
		newX = this.getDeltaX() * v;
		newY = this.getDeltaY() * v;
		return new Vector(newX, newY);
	}
	
	public Vector deflectY() {
		double newY;
		newY = this.getDeltaY() * -1;
		return new Vector(this.getDeltaX(), newY);
	}
	
	public Vector deflectX() {
		double newX;
		newX = this.getDeltaX() * -1;
		return new Vector(newX, this.getDeltaY());
	}
	
	public double magnitude() {
		double newX, newY;
		newX = this.getDeltaX() * this.getDeltaX();
		newY = this.getDeltaY() * this.getDeltaY();
		return Math.sqrt(newX + newY);
	}
	
	public Vector rescale(double v) {
		double newX, newY;
		if (this.magnitude() != 0) {
			newX = (this.getDeltaX() * v) / this.magnitude();
			newY = (this.getDeltaY() * v) / this.magnitude();
			return new Vector(newX, newY);
		}
		return new Vector(v, 0);
	}
	
}