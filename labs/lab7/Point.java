package lab7;



public class Point {
	
	private double xCoord, yCoord;
	
	public Point (double x, double y){
			
			xCoord = x;
			yCoord = y;
		
		}
	
	public double getX(){
		return xCoord;
	}
	
	public double getY() {
		return yCoord;
	}
	
	public Point plus(Vector v) {
		double newX, newY;
		newX = this.getX() + v.getDeltaX();
		newY = this.getY() + v.getDeltaY();
		return new Point(newX, newY);
	}
	
	public Vector minus(Point v) {
		double newX, newY;
		newX = this.getX() - v.getX();
		newY = this.getY() - v.getY();
		return new Vector(newX, newY);
	}
	
	public double distance(Point v) {
		double newX, newY;
		newX = this.getX() - v.getX();
		newY = this.getY() - v.getY();
		return Math.sqrt((newX)*(newX) + (newY)*(newY));
	}
	
}
