package lab3;

import cse131.ArgsProcessor;

public class dice {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ArgsProcessor ap = new ArgsProcessor(args);
		int D = ap.nextInt("How many Dice?");
		int T = ap.nextInt("How many times are the dice thrown?");
		int[] V = new int [D];
		int counter = 0;
		int[] total = new int [T];
		
		
		for (int j = 0; j < T; j++) {
			for (int i = 0; i < D; i++) {
				int random= (int)(6 * Math.random()) + 1;
				V[i] = random;
				
				
			}
			boolean allSame = true;
			int sum = 0;
			
			for (int i = 0; i < D; i++) {
				sum = V[i] + sum;
				
				if (V[i] != V[D-1]) {
					allSame = false;
					
				}
				
				System.out.println(V[i]);
				
			}
			total[j] = sum;
			
			if (allSame == true) {
				counter++;
			}
			
			System.out.println("Were they the same? " + allSame);
			System.out.println("The sum of the dice is " + sum);
			
//			
		}
		for (int i = 0; i < T; i++) {
			int totalCount = 1;
			for(int k = 0; k < T; k++) {
				if (total[i] == total[k] && i != k) {
					totalCount++;
				}
			}
			System.out.println("Sum " + total[i] + " was seen " + totalCount + " times");
		}
//		System.out.println(counter);
//		System.out.println(T);
		double percent =(double) counter /(double) T;
		System.out.println();
		System.out.println("The percentage of all dice being the same: " + percent);
//		System.out.println("The number of times the Sum was seen is " + totalCount);
	}

}
